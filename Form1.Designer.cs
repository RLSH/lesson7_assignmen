﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.yourScore = new System.Windows.Forms.Label();
            this.enemyScore = new System.Windows.Forms.Label();
            this.exitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // yourScore
            // 
            this.yourScore.AutoSize = true;
            this.yourScore.Location = new System.Drawing.Point(36, 27);
            this.yourScore.Name = "yourScore";
            this.yourScore.Size = new System.Drawing.Size(72, 13);
            this.yourScore.TabIndex = 0;
            this.yourScore.Text = "Your Score: 0";
            // 
            // enemyScore
            // 
            this.enemyScore.AutoSize = true;
            this.enemyScore.Location = new System.Drawing.Point(968, 27);
            this.enemyScore.Name = "enemyScore";
            this.enemyScore.Size = new System.Drawing.Size(104, 13);
            this.enemyScore.TabIndex = 1;
            this.enemyScore.Text = "Opponent\'s Score: 0";
            // 
            // exitButton
            // 
            this.exitButton.Location = new System.Drawing.Point(493, 12);
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(75, 23);
            this.exitButton.TabIndex = 2;
            this.exitButton.Text = "Forfeit";
            this.exitButton.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1120, 464);
            this.Controls.Add(this.exitButton);
            this.Controls.Add(this.enemyScore);
            this.Controls.Add(this.yourScore);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label yourScore;
        private System.Windows.Forms.Label enemyScore;
        private System.Windows.Forms.Button exitButton;
    }
}

