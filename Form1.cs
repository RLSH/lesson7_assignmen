﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private string serverMessage;

        public Form1()
        {
            InitializeComponent();
            this.BackColor = Color.FromArgb(25, 255, 23);
            createEnemyCard();
            exitButton.Enabled = true;

            //Thread waitToServer = new Thread(workWithTheServer);
            //waitToServer.Start();
            workWithTheServer();
            //Console.WriteLine(serverMessage);
        }

        private void createEnemyCard()
        {
            // create the image object itself
            System.Windows.Forms.PictureBox currentPic = new PictureBox();
            currentPic.Name = "enemyCard"; // in our case, this is the only property that changes between the different images
            currentPic.Image = global::WindowsFormsApp1.Properties.Resources.card_back_blue;
            currentPic.Location = new Point(480, 50);
            currentPic.Size = new System.Drawing.Size(100, 114);
            currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // add the picture object to the form (otherwise it won't be seen)
            this.Controls.Add(currentPic);
        }

        private void GenerateCards()
        {
            // set the location for where we start drawing the new cards (notice the location+height)
            Point nextLocation = new Point(10, 320);

            for (int i = 0; i < 10; i++)
            {
                // create the image object itself
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "picDynamic" + i; // in our case, this is the only property that changes between the different images
                currentPic.Image = global::WindowsFormsApp1.Properties.Resources.card_back_red;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(100, 114);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                // assign an event to it
                currentPic.Click += delegate (object sender1, EventArgs e1)
                {
                    string currentIndex = currentPic.Name.Substring(currentPic.Name.Length - 1);
                    MessageBox.Show("You've clicked card #" + currentIndex);

                    // use the delegate's params in order to remove the specific image which was clicked
                    ((PictureBox)sender1).Hide();
                    ((PictureBox)sender1).Dispose();
                };

                // add the picture object to the form (otherwise it won't be seen)
                this.Controls.Add(currentPic);

                // calculate the location point for the next card to be drawn/added
                nextLocation.X += currentPic.Size.Width + 11;
            }
        }

        private void workWithTheServer()
        {
            try
            {
                //Thread.Sleep(2000);
                /*MessageBox.Show("something something dark side");

                Invoke((MethodInvoker) delegate { txtMagshimim.Text = "some text from another thread"; });*/
                TcpClient client = new TcpClient();

                IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8220);

                client.Connect(serverEndPoint);

                MessageBox.Show("yas");

                NetworkStream clientStream = client.GetStream();

                

                byte[] bufferIn = new byte[567];
                int bytesRead = clientStream.Read(bufferIn, 0, 4);
                string input = new ASCIIEncoding().GetString(bufferIn);

                this.serverMessage = input;

                
            }
            catch(Exception e)
            {
                MessageBox.Show("did not work and i dont know why!!!!!!!!!!!!!!!!!!!!!!");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }
}
